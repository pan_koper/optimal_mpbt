import numpy as np

def prob(N, d, k):
    x = range(N-k+1, N+1)
    y = range(N + d**2 - k, N + d**2)
    return np.product(x)/np.product(y)

d = 2
k = 4
print("N,k")
for N in range (2,20):
    print(str(N), str(prob(N,d, k)), sep=",")
