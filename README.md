# Optimal_MPBT [![arXiv](https://img.shields.io/badge/arXiv-2011.09256-blue.svg?style=flat)](https://arxiv.org/abs/2011.09256)

The codes used in the study of performance of Optimal Multi Port Based Teleportation, in both deterministic and probabilistic scenario.

# Contents

`opt_k_star_fidelity.py` - Sage script for calculations of fidelity in the optimal deterministic scheme, basing on the computation of so-called teleportation matrix.

`p_succ_opt.py` - simple python script, evaluating the expression for the probability of success in optimal probabilistic scheme

`fidelity_d_x_k_y.txt` - results concerning the fidelity in `x` dimensional, `y` port deterministic case

`p_succ_d_x_k_y.txt` - results concerning the probability of success  in `x` dimensional, `y` port probabilistic case

# Usage

In Sage terminal type 
`>source(opt_k_star_fidelity.py)`

In case of second script simply run
`python3 p_succ_opt.py`

