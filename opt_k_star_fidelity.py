from itertools import chain


def SplitInLParts(n, l):
'''credits: Sergii Strelchuk, DAMTP Cambridge

function which generates partitions of n of size
at most d (we don't neet all of the partitions 
that built-in function #Partitions(N) gives us'''
    a = [0 for i in range(n + 1)]
    k = 1
    a[0] = 0
    a[1] = n
    while k != 0:
        x = a[k - 1] + 1
        y = a[k] - 1
        k -= 1
        while x <= y and k < l - 1:
            a[k] = x
            y -= x
            k += 1
        a[k] = x + y
        yield a[:k + 1]


def add_box(p, d):
'''credits: Sergii Strelchuk, DAMTP Cambridge'''
    #now add one more box keeping the height <=d and making sure that the partition is valid
    res = []
    v = list(p)
    Ft = 0 # current contribution to F by a single std tableaux

    for i in range(0,len(p)):
        if (i>0 and v[i-1]>=v[i]+1) or (i==0) : #check if we can add a box 
            v1 = list(v)
            v1[i] = v1[i]+1
            res.append(v1)
    if(len(p) < d):
        v1 = list(v)
        v1.append(1)
        res.append(v1)
        return res
    return res


def add_multiple_boxes(p_table, d, k):
    new = [add_box(pp, d) for pp in p_table]
    if (k==1): return list(chain(*new))
    
    res = []
    for p in new:
        res.append(add_multiple_boxes(p, d, k-1))
    return list(chain(*res))


def common_boxes(p1, p2):
    return sum([min(x,y) for x,y in zip(p1, p2)])


def tableaux_distance(p1, p2):
    if sum(p1) != sum(p2):
        return -1
    return sum(p1) - common_boxes(p1, p2)


def find_ways_from_alpha_to_mu(a, m, k, d):
    possible_diagrams=add_multiple_boxes([a], d, k)
    return possible_diagrams.count(m)


def gen_matrix(N, k, d):
    partitions = SplitInLParts(N, d)
    partitions = sorted([list(x)[::-1] for x in partitions])[::-1]

    partitions2 = SplitInLParts(N-k, d)
    partitions2 = sorted([list(x)[::-1] for x in partitions2])[::-1]

    M = [[0 for i in partitions] for i in partitions]
    for i in partitions:

        for j in partitions:

            if  -1 < tableaux_distance(i, j) <= k:

                entry = 0
                for a in partitions2:

                    entry += find_ways_from_alpha_to_mu(a, i, k, d) * find_ways_from_alpha_to_mu(a, j, k, d)

                M[partitions.index(i)][partitions.index(j)] = entry
    return M

def findFidelity(matr, d): #assuming quadratic matrix (given in Sage form)
    dim = matr.nrows()
    w = vector([lognormvariate(2,1) for i in range(dim)])
    w = w/sum(w) #initialize a random probability vector (LogNorm distribution)
    v = w
    rho_m = 0    
    for x in range(0,100):
        v = v*matr
        rho_m = sum(v)
        v = v/sum(v)
        #print ("SORTED: " + str(sorted(v)) + " RHO: " + str(rho_m) )
    #print(rho_m/d**2)
    return rho_m

    
d=2
k=2
for N in range (3, 20):
    m = gen_matrix(N, k, d)
    #print (m)
    mt = matrix(QQ, m)
    #print( mt.eigenvalues())
    r=findFidelity (mt, d)
    print(N, r/d**(2*k), sep=',')
print(m)
print(mt)

d=2
k=3
for N in range (4, 11):
    m = gen_matrix(N, k, d)
    #print (m)
    mt = matrix(QQ, m)
    #print( mt.eigenvalues())
    r=findFidelity (mt, d)
    print(N, r/d**(2*k))
print(m)
print(mt)
